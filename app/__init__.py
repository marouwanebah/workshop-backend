#from word_prediction import get_prediction
from flask import Flask, request
from flask import json
from backend.model.word_predit import get_prediction
app = Flask(__name__)

@app.route('/')
def hello_word():
    return "hello - word"


@app.route('/api/create/', methods=['POST'])
def createWithPost():
    nbMot = request.args.get('nb', None)
    text = request.args.get('text', None)

    response = app.response_class(
        response= json.dumps(get_prediction(int(nbMot), text)),
        status=200,
        mimetype='application/json'
    )
    #return  get_prediction(nbMot, text)
    return response

@app.route('/api/create/', methods=['GET'])
def createWithGet():
    nbMot = request.args.get('nb', None)
    text = request.args.get('text', None)
    print (nbMot)
    #return  get_prediction(nbMot, text)
    return 'success', 200

@app.route('/api/rap/', methods=['GET','POST'])
def getRapPrediction():
    nbMot = request.args.get('nb', None)
    text = request.args.get('text', None)
    print (nbMot)
    return  get_prediction(nbMot, text)
    return 'success', 200

if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
#app.run()
